package com.tasks.task2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CityService {
	
	private int [][]cityMap = {
			{0,1,3,0},
			{1,0,1,4},
			{3,1,0,1},
			{0,4,1,0}};  
	private List<String> names = new ArrayList<String>(); 	 // the collection of city names
	private List<Integer> path = new ArrayList<>();	// the collection consists of  one path of city indexes from the source city to the city of destination.
	private List<List<Integer>> paths = new ArrayList<>(); 	// the collection consists of  several path options of city indexes from the source city to the city of destination.
	private Scanner scanner = new Scanner(System.in);
	private int indexCitySource ; 
	private int indexCityDestin ;
	
	/*Method "init" fills a collection of city names, gives you to enter
	 * the current number of the source city and destination city from console*/
	public void init(){
		names.add("gdansk");
		names.add("bydgoszcz");
		names.add("torun");
		names.add("warszawa");
		System.out.println("Enter number source city  from list of follow city.");
		for (int i = 0; i < names.size(); i++) {
			System.out.println((i+1) +"."+ names.get(i));
		}
		System.out.print(":");
		
		if(scanner.hasNextInt()){  
			indexCitySource = scanner.nextInt()-1;
			/* the index of the source city  is assigned to variable "indexCitySource"
			 * (-1 because the consoles are entered  ordinal number, which is on "1" larger 
			 * than the ordinal number of this city in the collection.)*/
			
			System.out.println("Enter number destination city  from list of follow city.");
			for (int i = 0; i < names.size(); i++) {
				System.out.println((i+1) +"."+ names.get(i));
			}
			System.out.print(":");
			if(scanner.hasNextInt()){ 
				indexCityDestin = scanner.nextInt()-1; 
				 /* the index of the destination city  is assigned to
				  * variable "indexCityDestin"(-1 because the consoles are entered
				  * ordinal number, which is on "1" larger than the ordinal number
				  * of this city in the collection.)*/
			}else{
				System.out.println("Invalid input. Start again.");
			}
		}else{
			System.out.println("Invalid input. Start again.");
		}
	}
	
	/*This method writes data from the collection to a new collection.
	 * Copy current path for send to new recursion method.*/
	public List<Integer> copyPath(List<Integer> sourcePath){ 
		List<Integer> destinPath = new ArrayList<>();
		for (Integer sp : sourcePath) {
			destinPath.add(sp);
		}
		return destinPath;
	}
	
	/*This method fills of collection "paths" by other collections of all possible paths
	 * from the source city to the destination city (without visiting cities again.)*/
	public void checkCity(int indexCitySource, int indexCityDestin, List<Integer> currentPath){//"currentPath" contains the index of the source city
		for (int i = 0; i < names.size(); i++) {
			if(cityMap[indexCitySource][i]!=0 && !currentPath.contains(i)){ //if the cities have a connection (they are neighbors) AND  if this city we have not visited yet.
				List<Integer> newPath = copyPath(currentPath); //create a new collection, in which we copy the indexes of the cities we have visited.
				newPath.add(i);  // the index of the city, which we just visited, is added to the new collection.
				if(i==indexCityDestin){ 
					/*if the city we visited is the destination city, we add this  collection to the collection "paths",
					 * which contains of city indexes  through which we can get to the destination city*/
					paths.add(newPath);
				} else{ 		//if the index "i" is not an index of the destination city, then we start the recursion. 
				checkCity(i, indexCityDestin, newPath); 
				/* "i" is the index of the last visited city, "indexCityDestin" does not change,
				 * "newPath" is the collection that contains the indexes of all visited cities.
				 * Recursion continues until the  "currentPath.contains(i)" == true (when we have visited all the cities.)*/
				}
			}
		}
	}
	
	// This method returns the FULL cost of ONE path from the source city to the destination city.
	public int getCost(List<Integer> path){ // "List<Integer> path" - This is one element of the collection "paths".
		int cost = 0;
		for (int i = 0; i < path.size()-1; i++) {
			cost+=cityMap[path.get(i)][path.get(i+1)];
		}
		return cost;
	}
	
	/*This method returns a collection of the city indexes that make up the path
	 * from the source city to the destination city with a minimum cost. */
		public List<Integer> selectPathWithMinCost(){
			List<Integer> minPath = paths.get(0);    //imagine that the first way in the collection has a minimum cost.
			for (int i = 1; i < paths.size(); i++) {
				if(getCost(minPath)>getCost(paths.get(i))){ 
					/*if the cost of the next path is lower	than in the "minPath"
					 * collection, then "minPath" assigns a new value.*/
					minPath = paths.get(i);
				}
			}
			return minPath;
		}


	public static void main(String[] args) {
		CityService cityService = new CityService();
		int minCost = 0;  //variable with the minimum cost of the path.
		try{
			cityService.init();	
			List<Integer> beginPath =  new ArrayList<>(); // the collection which contains the index of the source city.
			beginPath.add(cityService.indexCitySource);
			cityService.checkCity(cityService.indexCitySource, cityService.indexCityDestin, beginPath);
			List<Integer> optimPath = cityService.selectPathWithMinCost(); /*collection (the "paths" collection element),which contains city indexes of the one path with a minimum cost.*/
			minCost = cityService.getCost(optimPath); 
			System.out.println("Min cost of path from "+ cityService.names.get(cityService.indexCitySource) +
					" to " + cityService.names.get(cityService.indexCityDestin) + " is  " + minCost);
		
		}catch(IndexOutOfBoundsException e){
			System.err.println("This city does not exist in the list. Start again.");
		}catch(Exception e){
			System.err.println(e);
		}
		
		
	}
}