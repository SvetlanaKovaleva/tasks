package com.tasks.task1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ValidBrackets {
	//Recursion method which creates correct expression of brackets
	public void createBracket(int rightBracket, int leftBracket, String s, List<String> brackets){
		if(rightBracket==0 && leftBracket==0){  // exit condition from recursion.
			brackets.add(s); //add string "s" with the left and the right brackets to the collection.
		} else{
			if(leftBracket>0){ 
				createBracket(rightBracket, leftBracket-1, s+"(", brackets);//recursion method which add left brackets to string "s".
			}
			if(rightBracket>leftBracket){
				createBracket(rightBracket-1, leftBracket, s+")", brackets);//recursion method which add right brackets to string "s".
			}
		}
	}
	public static void main(String[] args) {
		ValidBrackets validBrackets = new ValidBrackets();
		List<String> brackets = new ArrayList<String>();
		Scanner scanner = new Scanner(System.in);
		int numberBrackets;
		System.out.println("Enter the number of valid round brackets: ");
		if(scanner.hasNextInt()){
			numberBrackets = scanner.nextInt();
			validBrackets.createBracket(numberBrackets, numberBrackets, "", brackets);
			for (String bracket : brackets) {
				System.out.println(bracket);
			}
		}else{
			System.err.println("Invalid input. Start again.");
		}
		
	}

}
