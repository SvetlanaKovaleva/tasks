package com.tasks.task3;

import java.math.BigInteger;

public class TaskThird {

	public static void main(String[] args) {
		Factorial f = new Factorial();
		int digit = 100; //the integer that factorial sum of which we are looking for.
		BigInteger fact = f.fact(digit);
		BigInteger sumFact = f.sumFact(fact);
		System.out.println("Factorial of a number " + digit+ " = " + fact);
		System.out.println("Sum of the " + digit + "! = " + sumFact);

	}

}
