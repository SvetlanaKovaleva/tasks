package com.tasks.task3;

import java.math.BigInteger;

public class Factorial {
	//This method looks for the factorial of a number.
	public BigInteger fact(int i) {
		BigInteger result = null;
		if (i == 1)
			return BigInteger.valueOf(1);
		result = fact(i - 1).multiply(BigInteger.valueOf(i)); //recursion
		return result;
	}
	
	//This method calculates the sum of the factorial number.
	public BigInteger sumFact(BigInteger f) {
		BigInteger result = new BigInteger("0");
		BigInteger rem = new BigInteger("10");

		while (f.signum() == 1) { // if the factorial is a positive number
			BigInteger temp = f.remainder(rem); // division by module(temp = f%10).Get the last digit from the factorial
			result = result.add(temp); // result += temp
			f = f.divide(rem); // remove the last digit in the factorial
		}
		return result;
	}

}
